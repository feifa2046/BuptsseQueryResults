package com.buptsse.queryresults.bean;

import java.util.ArrayList;
import java.util.List;

public class SummaryInfo {

	public static final int ItemCount = 5;
	
	public static final int TYPE_TOTAL = 0;
	public static final int TYPE_OBTAIN = 1;
	public static final int TYPE_APLUS = 2;
	public static final int TYPE_F = 3;
	public static final int TYPE_C = 4;
	
	private float mTotalCredit;
	private float mObtainCredit;
	private float mAPlusLevelCredit;
	private float mFLevelCredit;
	private float mCLevelCredit;
	
	public List<SubjectInfo> mTotalList = new ArrayList<SubjectInfo>();
	public List<SubjectInfo> mObtainList = new ArrayList<SubjectInfo>();
	public List<SubjectInfo> mAPlusLevelList = new ArrayList<SubjectInfo>();
	public List<SubjectInfo> mFLevelList = new ArrayList<SubjectInfo>();
	public List<SubjectInfo> mCLevelList = new ArrayList<SubjectInfo>();
	
	public SummaryInfo(){}
	
	public SummaryInfo(List<SubjectInfo> subject){
		mTotalList = subject;
		for (int i = 0; i < subject.size(); i++) {
			mTotalCredit += subject.get(i).getSubjectCredit();
			if (subject.get(i).getSubjectLevel().equals(SubjectInfo.LEVEL_F)){
				mFLevelCredit += subject.get(i).getSubjectCredit();
				mFLevelList.add(subject.get(i));
			} else {
				mObtainList.add(subject.get(i));
				if (subject.get(i).getSubjectLevel().equals(SubjectInfo.LEVEL_A_PLUS)){
					mAPlusLevelCredit += subject.get(i).getSubjectCredit();
					mAPlusLevelList.add(subject.get(i));
				} else if (subject.get(i).getSubjectLevel().equals(SubjectInfo.LEVEL_C)){
					mCLevelCredit += subject.get(i).getSubjectCredit();
					mCLevelList.add(subject.get(i));
				} 
			}
		}
		mObtainCredit = mTotalCredit - mFLevelCredit;
	}
	
	public float getmTotalCredit() {
		return mTotalCredit;
	}
	public float getmObtainCredit() {
		return mObtainCredit;
	}
	public float getmAPlusLevelCredit() {
		return mAPlusLevelCredit;
	}
	public float getmFLevelCredit() {
		return mFLevelCredit;
	}
	public float getmCLevelCredit() {
		return mCLevelCredit;
	}

	public void setmTotalCredit(float mTotalCredit) {
		this.mTotalCredit = mTotalCredit;
	}

	public void setmObtainCredit(float mObtainCredit) {
		this.mObtainCredit = mObtainCredit;
	}

	public void setmAPlusLevelCredit(float mAPlusLevelCredit) {
		this.mAPlusLevelCredit = mAPlusLevelCredit;
	}

	public void setmFLevelCredit(float mFLevelCredit) {
		this.mFLevelCredit = mFLevelCredit;
	}

	public void setmCLevelCredit(float mCLevelCredit) {
		this.mCLevelCredit = mCLevelCredit;
	}
}