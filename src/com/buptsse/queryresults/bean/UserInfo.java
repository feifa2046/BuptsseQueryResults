package com.buptsse.queryresults.bean;

public class UserInfo {

	private static final int COUNTLIMIT = 3;
	
	private String userID;
	private String userName;
	private String userSex;
	
	public UserInfo(){}
	
	public UserInfo(String rawStr){
		String[] user = rawStr.split("-");
		if(user.length == COUNTLIMIT){
			userID = user[0];
			userName = user[1];
			userSex = user[2];
		}
	}
	
	public UserInfo(String id, String name, String sex){
		userID = id;
		userName = name;
		userSex = sex;
	}
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserSex() {
		return userSex;
	}
	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}
	
}
