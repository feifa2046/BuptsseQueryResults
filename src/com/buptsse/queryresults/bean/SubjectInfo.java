package com.buptsse.queryresults.bean;

public class SubjectInfo {

	public static final String LEVEL_A_PLUS = "A+";
	public static final String LEVEL_F = "F";
	public static final String LEVEL_C = "C";
	
	private static final int COUNTLIMIT = 5;
	
	private String subjectName;
	private float subjectScore;
	private String subjectLevel;
	private float subjectCredit;
	private String subjectDate;
	
	public SubjectInfo(){}
	
	public SubjectInfo(String rawStr){
		
		String[] subject = rawStr.split("，");
		
		if(subject.length == COUNTLIMIT){
			subjectName = subject[0];
			subjectScore = Float.parseFloat(subject[1]);
			subjectLevel = subject[2];
			subjectCredit = Float.parseFloat(subject[3]);
			subjectDate =  subject[4];
		}
	}
	
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public float getSubjectScore() {
		return subjectScore;
	}
	public void setSubjectScore(float subjectScore) {
		this.subjectScore = subjectScore;
	}
	public String getSubjectLevel() {
		return subjectLevel;
	}
	public void setSubjectLevel(String subjectLevel) {
		this.subjectLevel = subjectLevel;
	}
	public float getSubjectCredit() {
		return subjectCredit;
	}
	public void setSubjectCredit(float subjectCredit) {
		this.subjectCredit = subjectCredit;
	}
	public String getSubjectDate() {
		return subjectDate;
	}
	public void setSubjectDate(String subjectDate) {
		this.subjectDate = subjectDate;
	}
}
