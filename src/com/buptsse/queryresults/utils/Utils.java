package com.buptsse.queryresults.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.buptsse.queryresults.R;

public class Utils {
	
	private static final String TAG = "Utils";

	// ******************* Save Id Start *******************//
	private static final String SHARE_NAME = "my_id";
	private static final String SHARE_ID = "ID";

	public static void saveID(Context context, String id) {
		SharedPreferences msp = context.getSharedPreferences(SHARE_NAME,
				Context.MODE_APPEND);
		msp.edit().putString(SHARE_ID, id).commit();
	}

	public static String getID(Context context) {
		return context.getSharedPreferences(SHARE_NAME, Context.MODE_PRIVATE)
				.getString(SHARE_ID, "");
	}

	// ******************* Save Id End *******************//

	// ******************* Check Input Start *******************//
	public static String checkEdit(Context context, String str) {
		if (!TextUtils.isEmpty(str)) {
			boolean mat = str.matches("^[A-Za-z0-9]+$"); 
			if (mat) { return str; }
		}
		Toast.makeText(context, R.string.input_right_id, Toast.LENGTH_LONG).show();
		return null;
	}

	// ******************* Check Input End *******************//

	// ******************* Transfrom Html Start *******************//
	public static String getHtmlTable(String str) {
		return str.replace("\r\n", "");
	}

	public static String getInfo(String input) {
		String str = input
				.replaceAll("\r\n", " ")
				.replaceAll(
						"<td width = 42% ><font color=#FF0000>课程名称</font></td><td width = 15% ><font color=#FF0000>成绩</font></td><td width = 12% ><font color=#FF0000>等级</font></td><td width = 12% ><font color=#FF0000>学分</font></td><td width = 19% ><font color=#FF0000>考试时间</font></td><tr>",
						" ")
				.replaceAll(
						"<table border=1 width=70% align = center ><tr><td><font color=#0000FF>学号</font></td><td><font color=#0000FF>姓名</font></td><td><font color=#0000FF>性别</font></td></tr><tr>",
						"").replaceAll("</td><td>", " - ")
				.replaceAll("<tr><td width = 42% >", "")
				.replaceAll("</td><td width = 15% >", " \\| ")
				.replaceAll("</td><td width = 12% >", " \\| ")
				.replaceAll("</td><td width = 19% >", " \\| ")
				.replaceAll("</td></tr></table>", "\r\n")
				.replaceAll("\\&[a-zA-Z]{1,10};", "")
				.replaceAll("<[^>]*>", " ").replaceAll("[(/>)<]", " ")
				.replaceAll("返回首页", "").replaceAll("北京邮电大学软件学院研究生成绩查询", "")
//				.replaceAll("\r\n", "\r\n--------------------------\r\n")
				.replaceAll(" \\| ", "，").replaceAll(" ", "");
//		Log.d(TAG, "Str:"+str);
		return str;
	}

	// ******************* Transfrom Html End *******************//
	public static double width = 10;
	
	public static double countArcLength(double startX, double endX){
		if(startX > endX){
			return countCalculus(startX) - countCalculus(endX);
		}
		return countCalculus(endX) - countCalculus(startX);
	} 
	
	private static double countCalculus(double x){
		
		double result = 0; 
		double power = Math.pow( (-4*x/width), 8/3);
		double base = 8/3 * power * Math.pow(x, 3/5) * Math.exp(power);
		result = Math.sqrt(1 + Math.pow(base , 2));
		return result;
	}
}
