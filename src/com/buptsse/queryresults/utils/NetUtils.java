package com.buptsse.queryresults.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetUtils {

	private static final String TAG = "NetUtils";

	private static final String URL = "http://219.142.86.81/cjcx/yan/index.jsp";
	private static final String Referer = "http://219.142.86.81/cjcx/yan/cx.jsp";
	private static final String UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; MALC; InfoPath.2; .NET4.0C; .NET4.0E; Shuame)";

	// private static final String PadUserAgent =
	// "Mozilla/5.0(iPad; U; CPU OS 4_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8F191 Safari/6533.18.5";

	public static boolean isNetworkConnected(Context context) {
		if (context != null) {
			ConnectivityManager mConnectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetworkInfo = mConnectivityManager
					.getActiveNetworkInfo();
			if (mNetworkInfo != null) {
				return mNetworkInfo.isAvailable();
			}
		}
		return false;
	}

	public static String downloadResultHtml(String xuehao) {
		HttpClient hc = new DefaultHttpClient();
		HttpGet get = new HttpGet(Referer);
		try {
			HttpResponse hp = hc.execute(get);
			if(hp.getEntity().getContentLength() == 602){
				Log.i(TAG, "get success:" + hp.getStatusLine().getStatusCode());
			}else{
				return null;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		HttpPost post = new HttpPost(URL);
		post.setHeader("User-Agent", UserAgent);
		post.setHeader("Referer", Referer);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("xuehao", xuehao));
		nvps.add(new BasicNameValuePair("submit", "%B2%E9%D1%AF"));
		try {
			post.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
			HttpResponse resp = hc.execute(post);
			Log.i(TAG, "post code:" + resp.getStatusLine().getStatusCode());
			if (200 == resp.getStatusLine().getStatusCode()) {
				return EntityUtils.toString(resp.getEntity());
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
		/*finally {
			hc.getConnectionManager().closeExpiredConnections();
			hc.getConnectionManager().shutdown();
		}*/
	}

}
