package com.buptsse.queryresults.widgets;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.buptsse.queryresults.bean.SubjectInfo;
import com.buptsse.queryresults.bean.SummaryInfo;

public class SummaryListAdapter extends BaseAdapter {

	private Context mContext;
	private SummaryInfo mSummaryInfo;

	public SummaryListAdapter() {
	}

	public SummaryListAdapter(SummaryInfo summaryInfo, Context context) {
		mSummaryInfo = summaryInfo;
		mContext = context;
	}

	@Override
	public int getCount() {
		return mSummaryInfo.mTotalList.size() + SummaryInfo.ItemCount;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = new SummaryListItemView(mContext);
		}

		if (position > 4) {
			SubjectInfo subject = mSummaryInfo.mTotalList.get(position - 5);
			((SummaryListItemView) convertView).setContentText(
					subject.getSubjectName(), subject.getSubjectLevel(),
					subject.getSubjectScore() + "分");
		} else {
			switch (position) {
			case 0:
				((SummaryListItemView) convertView).setContentText("所修课程",
						mSummaryInfo.mTotalList.size() + "门",
						mSummaryInfo.getmTotalCredit() + "学分");
				break;
			case 1:
				((SummaryListItemView) convertView).setContentText("通过课程",
						mSummaryInfo.mObtainList.size() + "门",
						mSummaryInfo.getmObtainCredit() + "学分");
				break;
			case 2:
				((SummaryListItemView) convertView).setContentText("F 等课程",
						mSummaryInfo.mFLevelList.size() + "门",
						mSummaryInfo.getmFLevelCredit() + "学分");
				break;
			case 3:
				((SummaryListItemView) convertView).setContentText("C 等课程",
						mSummaryInfo.mCLevelList.size() + "门",
						mSummaryInfo.getmCLevelCredit() + "学分");
				break;
			case 4:
				((SummaryListItemView) convertView).setTitleText("成绩详情");
				break;
			default:
				break;
			}
		}
		return convertView;
	}

}
