package com.buptsse.queryresults.widgets;

import com.buptsse.queryresults.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SummaryListItemView extends LinearLayout{

	private TextView mTxtCName, mTxtCount, mTxtCredit;
	private Context mContext;
	
	public SummaryListItemView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public SummaryListItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public SummaryListItemView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
	}
	
	@Override
	protected void onFinishInflate() {
		// TODO Auto-generated method stub
		super.onFinishInflate();
	}
	
	public void setTitleText(String cname){
		setClickable(false);
		removeAllViews();
		mTxtCName = makeView(mContext, true);
		mTxtCName.setBackgroundColor(mContext.getResources().getColor(R.color.gray));
		mTxtCName.setText(cname);
		addView(mTxtCName);
	}
	
	public void setContentText(String cname, String count, String credit){
		setOrientation(HORIZONTAL);
		setClickable(false);
		mTxtCName = makeView(mContext, false);
		mTxtCount = makeView(mContext, false);
		mTxtCredit = makeView(mContext, false);
		removeAllViews();
		addView(mTxtCName);
		addView(mTxtCount);
		addView(mTxtCredit);
		setViewColor();
		mTxtCName.setText(cname);
		mTxtCount.setText(count);
		mTxtCredit.setText(credit);
	}
	
	private void setViewColor(){
		mTxtCName.setBackgroundColor(mContext.getResources().getColor(R.color.blue_deep));
		mTxtCount.setBackgroundColor(mContext.getResources().getColor(R.color.blue_light));
		mTxtCredit.setBackgroundColor(mContext.getResources().getColor(R.color.blue_light));
	}
	
	private TextView makeView(Context context, boolean isSingle){
		TextView txt = new TextView(context);
		txt.setTextSize(18);
		if(isSingle){
			txt.setLayoutParams(new LayoutParams(430,LayoutParams.MATCH_PARENT));
			txt.setGravity(Gravity.LEFT);
			txt.setPadding(15, 5, 5, 5);
		}else{
			txt.setLayoutParams(new LayoutParams(140,LayoutParams.MATCH_PARENT));
			txt.setPadding(5, 5, 5, 5);
			txt.setGravity(Gravity.CENTER);
		}
		txt.setTextColor(context.getResources().getColor(R.color.white));
		txt.setClickable(true);
		((LinearLayout.LayoutParams)txt.getLayoutParams()).leftMargin = 5;
		((LinearLayout.LayoutParams)txt.getLayoutParams()).bottomMargin = 5;
		return txt;
	}

}
